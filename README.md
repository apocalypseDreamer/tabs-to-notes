# Tabs to notes

This application is made to show user what notes occures in the given guitar tab.<br>
It also looks nice.

# Current version features

- Algorithm that calculates given tab to notes names, working both with guitar and the bass
- Whole app is written with ReactJS
- Everything is mobile friendly, was written in mobile-first strategy
- RWD all the things
- Aesthetic UI : unified colors, fonts, deliberate layout, modern CSS
- User friendly UX and cool animations thanks to CSSTransitionGroup
- _Random_ tab generator, thanks to which you can test the app without the need to have some guitar tab in your clipboard
- **How-to** section, opening on top of everything, where you can learn how to use this app (if you don't find it intuitive enough)
- How-to section is animated

## Hope you'll enjoy it

For any kind of suggestions or contact attempts, here's my [email][1]

[1]: "damntheguitars@gmail.com"

<br>

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
