import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './Result.css';
import { CSSTransition } from 'react-transition-group';

class Result extends Component {
  resultRef = React.createRef();

  findNote = (stringDefault, number) => {
    const noteIndex = this.props.notes.findIndex(
      note => stringDefault === note
    );
    const note = this.props.notes[noteIndex + number];

    return note;
  };

  handleString = (string, index, tabTuning, capoLocation) => {
    // finding index of tuning (given the notation)
    const tuningIndex = this.props.tunings.findIndex(
      tuning => tuning.notation.toUpperCase() === tabTuning.toUpperCase()
    );

    // creating a copy of an existing array and reversing it (the highest string as first)
    const tuningNotes = [...this.props.tunings[tuningIndex].notes].reverse();

    // the note while playing open string
    const defaultNote = tuningNotes[index];

    // every char from the string notation
    const chars = string.split('');

    // if there would be 2-digits notes, we will treat them as one
    let foundDouble = false;
    let stringToReturn = (
      <div className="line" key={index}>
        {chars.map((char, index) => {
          if (foundDouble) {
            foundDouble = false;
            return null;
          }

          if (index === 0) {
            return null;
          }

          if (char === '-') {
            return <div className="block empty" key={index} />;
          } else if (!isNaN(parseInt(char))) {
            if (!isNaN(parseInt(chars[index + 1]))) {
              foundDouble = true;
              const value = parseInt(char + chars[index + 1]);
              const result = this.findNote(defaultNote, value + capoLocation);
              return (
                <div
                  className="block note"
                  key={index}
                  style={{ minWidth: '80px' }}
                >
                  {result}
                </div>
              );
            } else {
              const value = parseInt(char);
              const result = this.findNote(defaultNote, value + capoLocation);
              return (
                <div className="block note" key={index}>
                  {result}
                </div>
              );
            }
          } else if (char === '|') {
            return <div className="block limiter" key={index} />;
          } else {
            return (
              <div className="block other" key={index}>
                {char}
              </div>
            );
          }
        })}
      </div>
    );

    return stringToReturn;
  };

  componentDidUpdate() {
    // once there would be any change in component, we need to set the justifyContent of result div
    // if there is nothing to be scrolled horizontally, everything can be justified to center and we can hide the scrollbar;
    // otherwise we need to set its justification to flex-start to avoid cutting some parts of the result

    const resultDiv = ReactDOM.findDOMNode(this).querySelector('#result');
    const scrollLeftMax = resultDiv.scrollLeftMax;

    resultDiv.style.justifyContent =
      scrollLeftMax === 0 ? 'center' : 'flex-start';

    resultDiv.style.overflowX = scrollLeftMax === 0 ? 'hidden' : 'scroll';
  }

  render() {
    const lines = this.props.preparedTab.map((line, index) =>
      this.handleString(line, index, this.props.tuning, this.props.capoLocation)
    );

    return (
      <div>
        <CSSTransition
          in={
            this.props.preparedTab.length !== 4 &&
            this.props.preparedTab.length !== 6
          }
          classNames="display-result"
          timeout={300}
          mountOnEnter
          unmountOnExit
        >
          <div className="error">Your tab must have 4 or 6 lines!</div>
        </CSSTransition>

        <CSSTransition
          in={
            this.props.preparedTab.length === 4 ||
            this.props.preparedTab.length === 6
          }
          classNames="display-result"
          timeout={300}
          mountOnEnter
          unmountOnExit
        >
          <div>
            <p>Tuning: {this.props.tuning}</p>
            <div id="result">
              <div className="result-wrapper">{lines}</div>
            </div>
          </div>
        </CSSTransition>
      </div>
    );
  }
}

export default Result;
