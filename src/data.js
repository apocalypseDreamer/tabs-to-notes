// importing all required images
import instr1 from './images/instr1.png';
import instr2 from './images/instr2.png';
import instr3 from './images/instr3.png';
import instr4 from './images/instr4.png';

const data = {
  tunings: [
    {
      name: 'E standard',
      notation: 'EADGBE',
      notes: ['E2', 'A2', 'D3', 'G3', 'B3', 'E4'],
      instrument: 'guitar'
    },

    {
      name: 'Eb standard',
      notation: 'EbAbDbGbBbEb',
      notes: ['D#2', 'G#2', 'C#3', 'F#3', 'A#3', 'D#4'],
      instrument: 'guitar'
    },

    {
      name: 'D standard',
      notation: 'DGCFAD',
      notes: ['D2', 'G2', 'C3', 'F3', 'A3', 'D4'],
      instrument: 'guitar'
    },

    {
      name: 'Drop D',
      notation: 'DADGBE',
      notes: ['D2', 'A2', 'D3', 'G3', 'B3', 'E4'],
      instrument: 'guitar'
    },

    {
      name: 'Drop Db/DropC#',
      notation: 'C#G#C#F#A#D#',
      notes: ['C#2', 'G#2', 'C#3', 'F#3', 'A#3', 'D#4'],
      instrument: 'guitar'
    },

    {
      name: 'Drop C',
      notation: 'CGCFAD',
      notes: ['C2', 'G2', 'C3', 'F3', 'A3', 'D4'],
      instrument: 'guitar'
    },

    {
      name: 'E standard BASS',
      notation: 'EADG',
      notes: ['E2', 'A2', 'D3', 'G3'],
      instrument: 'bass'
    },

    {
      name: 'Drop D BASS',
      notation: 'DADG',
      notes: ['D2', 'A2', 'D3', 'G3'],
      instrument: 'bass'
    }
  ],

  // prettier-ignore
  notes: [
        'C2', 'C#2', 'D2', 'D#2', 'E2', 'F2', 'F#2', 'G2', 'G#2', 'A2', 'A#2', 'B2',
        'C3', 'C#3', 'D3', 'D#3', 'E3', 'F3', 'F#3', 'G3', 'G#3', 'A3', 'A#3', 'B3',
        'C4', 'C#4', 'D4', 'D#4', 'E4', 'F4', 'F#4', 'G4', 'G#4', 'A4', 'A#4', 'B4',
        'C5', 'C#5', 'D5', 'D#5', 'E5', 'F5', 'F#5', 'G5', 'G#5', 'A5', 'A#5', 'B5',
        'C6', 'C#6', 'D6', 'D#6', 'E6', 'F6'
      ],

  listOfInstructions: [
    {
      text:
        "Find website with the tab you're looking for. Mind the tuning and capo informations. If they're not given, your tab is probably in E standard without capo.",
      image: instr1,
      id: 0
    },
    {
      text:
        "Copy the tab from which you want to know notes (and only the tab) to the clipboard. It must have either 4 or 6 lines (enters doesn't matter).",
      image: instr2,
      id: 1
    },
    {
      text:
        'Paste it into the text area. Then adjust the tuning and capo settings and click that Transform button.',
      image: instr3,
      id: 2
    },
    {
      text:
        'Have fun with the result! You can change anything in the input field or with tuning and capo settings. It will automatically change the result without any reloading.',
      image: instr4,
      id: 3
    }
  ],

  // prettier-ignore
  randomTabs: [
    'G|-2---2---------0h2---|--0--------------0h2---|\n\nD|-2-----2-----2-------|--2------2-----2-------|\n\nA|-0-------------------|--3--------------------|\n\nE|---------------------|-----------------------|\n',
    'e|---------------------------------------------------------------------------------|\n\nB|---------------------------------------------------------------------------------|\n\nG|-----1---1h2p1---1-------1---6\\4-1---6b8r6-4-6---2/4--4\\1-----------4------------|\n\nD|-2h4-----------4-----2h4-----------------------6----------2/4-4-4/6----4h6-4\\2-4-|\n\nA|---------------------------------------------------------------------------------|\n\nE|---------------------------------------------------------------------------------|\n',
    "G|-6/8-6------------6/8--6-----6/8--6/8-6-----------9/11--9-----6/8--6/8-6------|\n\nD|-------9-6------------------------------9--------------------------------9-6--|\n\nA|-----------4--4-4----------4-----------------7--7-7----------7----------------|\n\nE|------------------------------------------------------------------------------|\n",
    'e|-----5------8~~-----------3~~-----------------|\n\nB|-----5------8~~-----------3~~--------5-6p5----|\n\nG|-----7------10~-------------------------------|\n\nD|-----7------10~-------------------7-----------|\n\nA|--5----0-8--------7-5-0---3~~--0--5-----------|\n\nE|----------------------------------------------|\n',
    'e|---------------------------------------------------------------|\n\nB|---------------------------------------------------------------|\n\nG|---------------------------------------------------------------|\n\nD|--------1h3-1~-------------------------------------------------|\n\nA|------1--------3h5-3~-----1-3/5-3~----1h3-5-3h5-----------1----|\n\nE|--1h3-------------------------------------------3~----1h3--3~--|\n',
    'G|---------------------------------|---------------------------------|\n\nD|---------------------------------|---------------------------------|\n\nA|-------------------------4-2-1---|---------------------------------|\n\nD|-2-2-2-2-2-2-2-2-2-2-2-2-------6-|-6-6-6-6-6-6-6-6-6-6-6-6-6-4-2-1-|\n',
    'e|------------------10/12--12-12-12---12h15--12-12-12h15-12-12h15-12--12h5-----|\n\nB|--10--10-10-10-10------------------------------------------------------------|\n\nG|-----------------------------------------------------------------------------|\n\nD|-----------------------------------------------------------------------------|\n\nA|-----------------------------------------------------------------------------|\n\nE|-----------------------------------------------------------------------------|\n',
    'e|-----------------------------------------------------------------------------|\n\nb|-----------------------------------------------------------------------------|\n\ng|---------------------------------------------------------------------3h5p3-0-|\n\nD|-------------------1-1-1-------------------------------1-1-1h3p1-------------|\n\nA|---------------3-------3-1-1-1-1h3p1-----------------3-----------3-1---------|\n\nE|-1-1-1-3-4-4-6-----------------------3-1-1-1-3-4-4-6-------------------------|\n',
    'e|-------------------------------|--------------------------|\n\nB|-------------------------------|--------------------------|\n\nG|-------------------------------|--------------------------|\n\nD|-----0---4----0---4----0---4/--|-7---5-----4--------------|\n\nA|-0h2---2----2---2----2---2-----|-0-------------5----------|\n\nE|-------------------------------|-----------3--------3-----|\n',
    'G|-----------------------------|\n\nD|---10-10-5-6-----8-10-5-6----|\n\nA|-----------------------------|\n\nE|--8--8----------6-8----------|\n'
  ]
};

export default data;
