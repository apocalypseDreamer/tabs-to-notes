import React from 'react';
import './Instructions.css';

const instructions = props => {
  return (
    <div className="page-wrapper" onClick={props.click}>
      <div className="instructions-wrapper">
        <div
          className="nav-arrow"
          id="arrowLeft"
          onClick={() => props.changeInstructions('left')}
        >
          <i className="fas fa-arrow-circle-left" />
        </div>

        <div className="instructions-slider">
          <div
            className="instructions-slider-wrapper"
            // we're translating the slider wrapper according to which slide should be displayed right now
            style={{
              transform: `translateX(-${props.current * 100}%)`
            }}
          >
            {props.instructions.map((instr, index) => (
              <div
                className={`instruction-container ${
                  // if the index of instruction element is the same as current (set in state) instructionId
                  // the div also gets "active" class
                  index === props.current ? 'active' : ''
                }`}
                key={instr.id}
              >
                <div className="image-container">
                  <img className="instruction-image" src={instr.image} alt="" />
                </div>

                <div className="instruction-text-container">
                  <p className="instruction-text">{instr.text}</p>
                </div>
              </div>
            ))}
          </div>
        </div>

        <div
          className="nav-arrow"
          id="arrowRight"
          onClick={() => props.changeInstructions('right')}
        >
          <i className="fas fa-arrow-circle-right" />
        </div>
      </div>
    </div>
  );
};

export default instructions;
