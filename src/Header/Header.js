import React from 'react';
import './Header.css';

const links = [
  {
    platform: 'GitHub',
    icon: <i className="fab fa-github" />,
    username: 'apocalypseDreamer',
    link: 'https://github.com/apocalypseDreamer'
  },

  {
    platform: 'GitLab',
    icon: <i className="fab fa-gitlab" />,
    username: 'apocalypseDreamer',
    link: 'https://gitlab.com/apocalypseDreamer'
  },
  {
    platform: 'Mail',
    icon: <i className="far fa-envelope" />,
    username: 'damntheguitars@gmail.com',
    link: 'mailto:damntheguitars@gmail.com'
  }
];

const header = props => {
  return (
    <div className="header-wrapper">
      <div className="header-child header-howto" onClick={props.click}>
        <h3>How to use?</h3>
      </div>

      <div className="header-child header-main">
        <h1>Tabs to notes</h1>
      </div>

      <div className="header-child header-contact">
        <h3>Contact me!</h3>
        <ul className="contact-menu">
          {links.map(el => (
            <li className="contact-item" key={el.platform}>
              <a className="contact-link" href={el.link} target="_blank" rel="noopener noreferrer">
                {el.icon} {el.platform} <br />
                <span className="contact-username">{el.username}</span>
              </a>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default header;
