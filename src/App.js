import React, { Component } from 'react';
import './App.css';

// all the data using in our app
import data from './data';

// all the using components
import Header from './Header/Header';
import Form from './Form/Form';
import Result from './Result/Result';
import Instructions from './Instructions/Instructions';

import { CSSTransition } from 'react-transition-group';

class App extends Component {
  state = {
    tunings: data.tunings,
    notes: data.notes,
    listOfInstructions: data.listOfInstructions,

    currentInstructionId: 0,

    userInput: null,
    preparedTab: null,

    // default ones
    tuning: 'EADGBE',
    instrument: 'guitar',
    isCapo: false,
    capoLocation: 0,

    toggleSettings: false,
    toggleButton: false,
    toggleResult: false,
    toggleInstructions: false,

    // we need it to be shown only once, then it wont be needed
    buttonShown: false,

    // were creating reference to tuning select dropdown, so we can take its value when we need it
    tuningReference: React.createRef(),

    // same thing with capoLocation input
    capoLocationReference: React.createRef()
  };

  randomizeTab = () => {
    const tab =
      data.randomTabs[Math.floor(Math.random() * data.randomTabs.length)];
    document.querySelector('#tabArea').value = tab;

    this.setState(
      {
        toggleSettings: false,
        toggleResult: false
      },
      () => {
        const tabArr = this.prepareTab(tab);
        this.updateTab(tabArr, tab);
      }
    );
  };

  prepareTab = input => {
    const tabArr = input.split(/\n/);

    // search for empty lines in given input
    const emptyLines = [];
    tabArr.forEach((line, index) => {
      if (line === '') {
        emptyLines.push(index);
      }
    });

    // get rid of empty lines in array
    while (emptyLines.length) {
      tabArr.splice(emptyLines.pop(), 1);
    }

    return tabArr;
  };

  updateTab = (tabArr, input) => {
    // checking instrument
    if (tabArr.length === 4) {
      this.setState({
        instrument: 'bass'
      });
    } else if (tabArr.length === 6) {
      this.setState({
        instrument: 'guitar'
      });
    }

    this.setState(
      {
        userInput: input,
        preparedTab: tabArr,
        toggleSettings: true,
        // if it wasn't displayed already, we can toggle it
        toggleButton: !this.state.buttonShown ? true : false,
        toggleResult: this.state.buttonShown ? true : false
      },
      // once we update preparedTab, we need to check if the tuning didn't changed (happens when changing length of tab)
      // if so, we update it as well
      () => {
        if (
          (this.state.preparedTab.length === 4 ||
            this.state.preparedTab.length === 6) &&
          this.state.tuningReference.current
        ) {
          const newTuning = this.state.tuningReference.current.value;
          this.tuningChanger(newTuning);
        }
      }
    );
  };

  handleUserInput = e => {
    const input = e.target.value;

    const tabArr = this.prepareTab(input);

    this.updateTab(tabArr, input);
  };

  // DRY - we are using this changer twice, so now it's a separate method
  tuningChanger = newTuning => {
    const indexOfNewTuning = this.state.tunings.findIndex(
      tuning => tuning.name === newTuning
    );
    const newTuningNotation = this.state.tunings[indexOfNewTuning].notation;
    this.setState({
      tuning: newTuningNotation
    });
  };

  handleTuningChange = e => {
    const newTuning = e.target.value;
    this.tuningChanger(newTuning);
  };

  handleCapoSetter = () => {
    const currentState = this.state.isCapo;
    // when it is set we need to disable it, and clear capoLocation value
    if (currentState) {
      this.setState({
        isCapo: false,
        capoLocation: 0
      });
    } else {
      this.setState({
        isCapo: true
      });

      // when we're setting isCapo to true, we also check if there is something already in the location input
      // and if it's a number, we're also updating the state of location
      const capoReference = this.state.capoLocationReference.current;
      if (!isNaN(parseInt(capoReference.value))) {
        this.setState({
          capoLocation: parseInt(capoReference.value)
        });
      }
    }
  };

  handleCapoLocation = e => {
    const newLocation = e.target.value;

    // check if user input is valid
    if (!isNaN(parseInt(newLocation)) && this.state.isCapo) {
      this.setState({
        capoLocation: parseInt(newLocation)
      });
      // if no, the location is gonna be set to default
    } else {
      this.setState({
        capoLocation: 0
      });
    }
  };

  handleTransformButton = () => {
    // we can transform the tab when its already given
    if (this.state.userInput) {
      this.setState({
        toggleResult: true,
        buttonShown: true,
        toggleButton: false
      });
    }
  };

  closeInstructions = e => {
    if (e.target.classList.contains('page-wrapper')) {
      this.setState(
        {
          toggleInstructions: false
        },
        () => {
          // we need to clear the overflow-y: hidden rule
          document.body.style = '';
        }
      );
    }
  };

  instructionsToggler = () => {
    this.setState(
      {
        toggleInstructions: true,
        currentInstructionId: 0
      },
      () => {
        // when the instructions popup is open, user can't scroll over the page, which is quite intuitive
        document.body.style.overflowY = 'hidden';
      }
    );
  };

  changeInstructions = direction => {
    if (direction === 'left') {
      if (this.state.currentInstructionId !== 0) {
        const newId = this.state.currentInstructionId - 1;
        this.setState({
          currentInstructionId: newId
        });
      }
    } else if (direction === 'right') {
      if (
        this.state.currentInstructionId !==
        this.state.listOfInstructions.length - 1
      ) {
        const newId = this.state.currentInstructionId + 1;
        this.setState({
          currentInstructionId: newId
        });
      }
    }
  };

  render() {
    return (
      <div className="App">
        <Header click={this.instructionsToggler} />
        <Form
          tunings={this.state.tunings}
          instrument={this.state.instrument}
          toggleSettings={this.state.toggleSettings}
          toggleButton={this.state.toggleButton}
          changeInput={e => this.handleUserInput(e)}
          tuningChange={e => this.handleTuningChange(e)}
          capoSetter={this.handleCapoSetter}
          capoLocation={e => this.handleCapoLocation(e)}
          toggleResult={this.handleTransformButton}
          tuningReference={this.state.tuningReference}
          capoReference={this.state.capoLocationReference}
          preparedTab={this.state.preparedTab}
          randomizeTab={this.randomizeTab}
        />

        <CSSTransition
          in={this.state.toggleResult}
          timeout={200}
          classNames="result"
          mountOnEnter
        >
          <div>
            <Result
              preparedTab={this.state.preparedTab}
              tuning={this.state.tuning}
              tunings={this.state.tunings}
              isCapo={this.state.isCapo}
              capoLocation={this.state.capoLocation}
              notes={this.state.notes}
            />
          </div>
        </CSSTransition>

        <CSSTransition
          in={this.state.toggleInstructions}
          classNames="instructions"
          timeout={500}
          mountOnEnter
          unmountOnExit
        >
          <Instructions
            click={e => this.closeInstructions(e)}
            changeInstructions={direction => this.changeInstructions(direction)}
            instructions={this.state.listOfInstructions}
            current={this.state.currentInstructionId}
          />
        </CSSTransition>
      </div>
    );
  }
}

export default App;
