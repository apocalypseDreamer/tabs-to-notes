import React from 'react';
import { CSSTransition } from 'react-transition-group';
import './Form.css';

const form = props => {
  const tuningList = props.tunings.filter(tuning => {
    return tuning.instrument === props.instrument;
  });

  const optList = tuningList.map(tuning => {
    return (
      <option key={tuning.notation} value={tuning.name}>
        {tuning.name}
      </option>
    );
  });

  return (
    <div>
      <div className="form-wrapper">
        <h2 style={{ marginTop: '30px' }}>Enter your tab below: </h2>
        <textarea
          rows="12"
          cols="120"
          id="tabArea"
          onChange={props.changeInput}
        />
        <h3 className="random-header">
          Or try out some{' '}
          <span className="generate-tab" onClick={props.randomizeTab}>
            random
          </span>{' '}
          tab
        </h3>
        <br />

        <CSSTransition
          in={
            props.toggleSettings &&
            (props.preparedTab.length === 4 || props.preparedTab.length === 6)
          }
          timeout={300}
          classNames="display-config"
          mountOnEnter
          unmountOnExit
        >
          <div className="config-form">
            <div className="form-line">
              Tuning:{' '}
              <select
                id="tuningSelect"
                onChange={props.tuningChange}
                onLoad={props.tuningChange}
                ref={props.tuningReference}
              >
                {optList}
              </select>
            </div>
            <div className="form-line">
              <label className="checkbox-container">
                Capo?
                <input
                  type="checkbox"
                  name="capo"
                  id="isCapo"
                  onChange={props.capoSetter}
                />
                <span className="checkmark" />
              </label>
              <input
                type="text"
                name="capo"
                id="capoLocation"
                placeholder="Which fret?"
                onChange={props.capoLocation}
                ref={props.capoReference}
              />
            </div>
          </div>
        </CSSTransition>
      </div>

      <CSSTransition
        in={props.toggleButton}
        timeout={{
          enter: 400,
          exit: 200
        }}
        classNames="display-btn"
        mountOnEnter
        unmountOnExit
      >
        <div>
          <div id="submitBtn" onClick={props.toggleResult}>
            Transform!
          </div>
        </div>
      </CSSTransition>
    </div>
  );
};

export default form;
